<?php

use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;

if (!function_exists('dd')) {
    /**
     * Dump the passed variables and end the script.
     *
     * @param  mixed
     * @return void
     */
    function dd()
    {
        $dumper = new HTMLDumper();
        $dumper->setStyles([
            'default' => 'background-color: #1d1f21; color: #c5c8c6',
            'num' => 'color:#de935f',
            'const' => 'color:#de935f',
            'str' => 'color:#b5bd68',
            'cchr' => 'color:#b5bd68',
            'note' => 'color:#b294bb',
            'ref' => 'color:#81a2be',
            'public' => 'color:#cc6666',
            'protected' => 'color:#cc6666',
            'private' => 'color:#cc6666',
            'meta' => 'color:#b294bb',
            'key' => 'color:#b294bb',
            'index' => 'color:#b294bb',
        ]);
        $cloner = new VarCloner();

        ob_start();
        array_map(function ($x) use ($dumper, $cloner) {
            $dumper->dump($cloner->cloneVar($x));
        }, func_get_args());

        $output = ob_get_clean();

        throw new \Exception($output, 418);
    }
}
