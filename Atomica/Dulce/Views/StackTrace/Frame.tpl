<div class="card is-fullwidth">
    <header class="card-header">
        <p class="card-header-title">
            <?= $shortFilename ?> @ <?= $line ?>
        </p>
    </header>
    <div class="card-content" style="background-color: #1d1f21">
        <?php if (count($lines)): ?>
            <div class="content">
                <?php foreach ($lines as $line): ?>
                    <div class="columns is-mobile trace--line trace--line__active-<?= $line['activeLine'] ? 'true' : 'false' ?>">
                        <div class="column is-narrow trace--line__number">
                            <a href="<?= $line['href'] ?>"><?= $line['number'] ?></a>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="column is-11 trace--line__source">
                            <pre><code class="php"><?= $line['source'] ?></code></pre>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        <?php else: ?>
            <div class="content">
                <tr class="warning" style="border-left: 2px solid #000; border-right: 2px solid #000;">
                    <td colspan="2" class="text-muted">runtime function / trace not available for this frame</td>
                </tr>
            </div>
        <?php endif ?>
    </div>
    <footer class="card-footer">
        <a class="card-footer-item" href="<?= $editorUri ?>">Open In IntelliJ</a>
    </footer>
</div>
