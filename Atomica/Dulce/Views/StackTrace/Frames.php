<?php

declare(strict_types = 1);

namespace Atomica\Dulce\Views\StackTrace;

use Atomica\Bluestone\Contracts\View;
use Atomica\Dulce\DulceErrorException;
use Atomica\Dulce\Views\Filters\FilterDulce;

class Frames implements View
{
    /** @var FilterDulce */
    private $filterDulce;


    public function __construct(FilterDulce $filterDulce)
    {
        $this->filterDulce = $filterDulce;
    }


    public function prepare(array $hints = []) : array
    {
        /** @var \Throwable $throwable */
        $throwable = $hints['throwable'];

        $frames = $throwable->getTrace();
        array_unshift($frames, [
            'file' => $throwable->getFile(),
            'line' => $throwable->getLine(),
        ]);
        $frames = array_filter($frames, [$this->filterDulce, 'filter']);

        // FIXME: Ugly hack to get rid of the error handler and the duplicate error frame
        if ($throwable instanceof DulceErrorException) {
            array_shift($frames);
            array_shift($frames);
        }

        return [
            'message' => $throwable->getMessage(),
            'file' => $throwable->getFile() ?? 'not available',
            'line' => $throwable->getLine() ?? 'not available',
            'code' => $throwable->getCode() ?? 503,
            'frames' => $frames,
        ];
    }
}
