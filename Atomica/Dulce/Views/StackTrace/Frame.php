<?php

declare(strict_types = 1);

namespace Atomica\Dulce\Views\StackTrace;

use Atomica\Bluestone\Contracts\View;
use Atomica\Dulce\HighlightSnippet;

class Frame implements View
{
    /** @var HighlightSnippet */
    private $highlightSnippet;


    public function __construct(HighlightSnippet $highlightSnippet)
    {
        $this->highlightSnippet = $highlightSnippet;
    }


    public function prepare(array $hints = []) : array
    {
        $frame = $hints['frame'];
        $query = http_build_query([
            'file' => $frame['file'],
            'line' => $frame['line'],
        ]);
        return [
            'editorUri' => 'idea://open?' . $query,
            'shortFilename' => $frame['file'],
            'file' => $frame['file'],
            'line' => $frame['line'],
            'lines' => $this->highlightSnippet->highlightSnippet($frame['file'], $frame['line'], 11),
        ];
    }
}
