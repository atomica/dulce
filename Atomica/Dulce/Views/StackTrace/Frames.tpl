<style>
    pre, code, pre code, .trace--line, .trace--line__number, .trace--line__source {
        padding: 0 !important;
        margin: 0 !important;
        line-height: 18px !important;
        font-size: 16px !important;
    }
    .trace--line {
        padding: 1px 0 !important;
    }
    .trace--line__number a, .trace--line__number a:visited, .trace--line__number a:hover {
        border: none !important;
    }
    .trace--line__number a:hover {
        color: #fff;
    }
    .trace--line__active-true, .trace--line__active-true .trace--line__source pre code {
        background-color: #31312e !important;
    }
    .sf-dump {
        padding: 5px 5px !important;
    }
    .sf-dump-expanded {
        overflow-wrap: break-word !important;
    }
</style>

<div class="container is-fluid">
    <?php foreach ($frames as $frame): ?>
        <?= $this->render(\Atomica\Dulce\Views\StackTrace\Frame::class, ['frame' => $frame]); ?>
        <br>
    <?php endforeach; ?>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.7.0/highlight.min.js" integrity="sha256-s63qpgPYoQk+wv3U6WZqioVJrwFNBTgD4dkeegLuwvo=" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.7.0/languages/php.min.js" integrity="sha256-GRXMU+6LhnGd+e5/EWBdAS8EekJQISjEuK/IMZiPBTU=" crossorigin="anonymous"></script>
<script>hljs.initHighlightingOnLoad();</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.7.0/styles/tomorrow-night.min.css" integrity="sha256-0QU8ry64q+N6YBIEF/6XF6vUeF15gbNO4tLS6ikk0FI=" crossorigin="anonymous"/>
