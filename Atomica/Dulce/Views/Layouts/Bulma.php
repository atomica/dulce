<?php

declare(strict_types = 1);

namespace Atomica\Dulce\Views\Layouts;

use Atomica\Bluestone\Contracts\View;

class Bulma implements View
{
    public function prepare(array $hints = []) : array
    {
        return $hints;
    }
}
