<html>
<head>
    <title><?= $title ?></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.2.3/css/bulma.min.css" integrity="sha256-F7gqKszCwmz8vhiti+AICU8dLfIEpxzPVihhhGfbbKg=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata|Geo" crossorigin="anonymous">
    <?= $this->section('styles') ?>
</head>
<body>

<?= $this->section('content') ?>

<div class="container">
    <br>
    <div class="has-text-right" style="color: #3a6797; font-family: 'Geo', sans-serif; font-size: larger;">END OF LINE.</div>
</div>
<br><br>
<?= $this->section('scripts') ?>
</body>
</html>
