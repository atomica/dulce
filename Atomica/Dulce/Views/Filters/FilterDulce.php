<?php

declare(strict_types = 1);

namespace Atomica\Dulce\Views\Filters;

use Atomica\Dulce\Contracts\Filter;

class FilterDulce implements Filter
{
    public function filter($frame) : bool
    {
        if (!isset($frame['file'])) {
            return false;
        }

        $paths = [
            join(DIRECTORY_SEPARATOR, ['vendor']),
            join(DIRECTORY_SEPARATOR, ['dulce', 'functions']),
            join(DIRECTORY_SEPARATOR, ['Atomica', 'Dulce']),
        ];

        foreach ($paths as $path) {
            if (strpos($frame['file'], $path) !== false) {
                return false;
            }
        }

        return true;
    }
}
