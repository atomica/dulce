<?php

declare(strict_types = 1);

namespace Atomica\Dulce\Views\Pages;

use Atomica\Bluestone\Contracts\View;

class Throwable implements View
{
    public function prepare(array $hints = []) : array
    {
        /** @var \Throwable $throwable */
        $throwable = $hints['throwable'];

        $message = $throwable->getMessage();
        $variableDump = null;

        if ($throwable->getCode() == 418) {
            $message = 'Variable Dump';
            $variableDump = $throwable->getMessage();
        }

        return [
            'message' => $message,
            'variableDump' => $variableDump,
            'file' => $throwable->getFile(),
            'line' => $throwable->getLine(),
            'throwable' => $throwable,
        ];
    }
}
