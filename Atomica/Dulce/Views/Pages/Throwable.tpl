<?php $this->layout(\Atomica\Dulce\Views\Layouts\Bulma::class, ['title' => $message]); ?>

<?php $this->startSection('content') ?>
<section class="hero is-dark">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        <?= $message ?>
      </h1>
      <h2 class="subtitle">
        <?= $file ?> : <?= $line ?>
      </h2>
    </div>
  </div>
</section>

<?php if (isset($variableDump)): ?>
    <div class="card is-fullwidth">
        <?= $variableDump ?>
    </div>
<?php endif ?>
<br>
<?= $this->render(\Atomica\Dulce\Views\StackTrace\Frames::class, ['throwable' => $throwable]); ?>

<?php $this->endSection('content') ?>

