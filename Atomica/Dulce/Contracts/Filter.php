<?php

declare(strict_types = 1);

namespace Atomica\Dulce\Contracts;

interface Filter
{
    public function filter($frame) : bool;
}
