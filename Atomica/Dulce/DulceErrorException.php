<?php

declare(strict_types = 1);

namespace Atomica\Dulce;

class DulceErrorException extends \ErrorException
{
}
