<?php

declare(strict_types = 1);

namespace Atomica\Dulce;

use Atomica\Bluestone\Bluestone;
use Atomica\Dulce\Views\Pages\Throwable;

class ErrorHandler
{
    /** @var Bluestone */
    private $bluestone;


    public function __construct(Bluestone $bluestone)
    {
        $this->bluestone = $bluestone;
    }


    public function handleError(int $errno, string $errstr, string $errfile, int $errline, array $errcontext): bool
    {
        $this->sendResponseCode(503);
        $exception = new DulceErrorException($errstr, $errno, 1, $errfile, $errline);
        echo $this->bluestone->render(Throwable::class, ['throwable' => $exception]);
        die;
    }


    public function handleThrowable(\Throwable $throwable)
    {
        $this->sendResponseCode($throwable->getCode());
        echo $this->bluestone->render(Throwable::class, ['throwable' => $throwable]);
        die;
    }


    private function sendResponseCode($code)
    {
        if ($code < 500 || $code > 599) {
            $code = 503;
        }
        http_response_code($code);
    }
}
