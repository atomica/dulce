<?php
use Atomica\Bluestone\Contracts\Engine;
use Atomica\Bluestone\Engines\Php;
use Atomica\Dulce\ErrorHandler;

require join(DIRECTORY_SEPARATOR, [__DIR__, '..', 'vendor', 'autoload.php']);

$builder = new \DI\ContainerBuilder();
$builder->addDefinitions([
    Engine::class => function (\Interop\Container\ContainerInterface $container) {
        return new Php($container);
    },
]);
$container = $builder->build();

set_error_handler(function (...$args) use ($container) {
    $errorHandler = $container->get(ErrorHandler::class);
    $container->call([$errorHandler, 'handleError'], $args);
});

set_exception_handler(function (...$args) use ($container) {
    $errorHandler = $container->get(ErrorHandler::class);
    $container->call([$errorHandler, 'handleThrowable'], $args);
});

function testing() {
    throw new \Exception('Testing Exceptions');
}

//dd($_SERVER);
//$something = $_SERVER[0];
testing();
